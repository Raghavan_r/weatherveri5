package com.app.weather.gpstracker

import android.Manifest
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import androidx.core.content.ContextCompat
import com.app.weather.listener.LocationUpdateListener


// The minimum distance to change Updates in meters
private const val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 10 // 10 meters

// The minimum time between updates in milliseconds
private const val MIN_TIME_BW_UPDATES = 5000 // 1 minute
    .toLong()

/**
 * This class is used to get the current location of the app
 * */
@Suppress("EmptyDefaultConstructor")
class GpsTracker() : Service(), LocationListener {

    private lateinit var mContext: Context
    private var locationUpdateListener: LocationUpdateListener? = null

    // flag for GPS status
    private var isGPSEnabled = false

    // flag for network status
    private var isNetworkEnabled = false

    // flag for GPS status
    private var canGetLocation = false

    private var location: Location? = null
    private var latitude = 0.0
    private var longitude = 0.0

    // Declaring a Location Manager
    protected var locationManager: LocationManager? = null

    constructor(context: Context, locationUpdateListener: LocationUpdateListener) : this() {
        this.mContext = context
        this.locationUpdateListener = locationUpdateListener
    }

    init {
        getLoc()
    }

    /**
     * This method get the Current location using @property locationManager and GPS
     * */
    @Suppress("ComplexMethod", "NestedBlockDepth")
    fun getLoc(): Location? {

        try {
            if (ContextCompat.checkSelfPermission(
                    mContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                locationManager = mContext
                    .getSystemService(Context.LOCATION_SERVICE) as LocationManager
                // getting GPS status
                locationManager.let {
                    if (it != null) {
                        isGPSEnabled = it.isProviderEnabled(LocationManager.GPS_PROVIDER)

                        isNetworkEnabled = it
                            .isProviderEnabled(LocationManager.NETWORK_PROVIDER)

                        if (isGPSEnabled || isNetworkEnabled) {

                            canGetLocation = true
                            if (isNetworkEnabled) {
                                it.requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                                )

                                Log.d("Network", "Network")

                                if (locationManager != null) {
                                    location =
                                        it.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)

                                    updateLatLng()
                                }
                            }

                            // if GPS Enabled get lat/long using GPS Services
                            if (isGPSEnabled && location == null) {
                                it.requestLocationUpdates(
                                    LocationManager.GPS_PROVIDER,
                                    MIN_TIME_BW_UPDATES,
                                    MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                                )
                                Log.d("GPS Enabled", "GPS Enabled")

                                if (locationManager != null) {
                                    location = it
                                        .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                                    updateLatLng()
                                }
                            }
                        }
                    }
                }
            }
        } catch (e: Exception) {

        }
        return location
    }

    private fun updateLatLng() {
        location.let { location ->

            if (location != null) {
                latitude = location.latitude
                longitude = location.longitude
            }
        }
    }

    /**
     * Stop using GPS listener Calling this function will stop using GPS in your
     * app.
     */
    fun stopUsingGPS() {
        locationManager?.removeUpdates(this)
    }

    /**
     * Function to get latitude
     */
    fun getLat(): Double {

        this.location.let {
            if (it != null) {
                latitude = it.latitude
            }
        }
        return latitude
    }

    /**
     * Function to get longitude
     */
    fun getLong(): Double {
        this.location.let {
            if (it != null) {
                longitude = it.longitude
            }
        }
        return longitude
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */
    fun canGetLocation(): Boolean = canGetLocation

    override fun onBind(p0: Intent?): IBinder? {
        TODO("not implemented")
    }

    override fun onLocationChanged(p0: Location?) {
        locationUpdateListener.let { locationUpdateListener ->
            if (p0 != null) {
                locationUpdateListener?.updatedLocation(p0)
            }
        }
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
        if (!p0.isNullOrEmpty()) {
            Log.d("Status changed", p0)
        }
    }

    override fun onProviderEnabled(p0: String?) {
        if (!p0.isNullOrEmpty()) {
            Log.d("Status changed", p0)
        }
    }

    override fun onProviderDisabled(p0: String?) {
        if (!p0.isNullOrEmpty()) {
            Log.d("Status changed", p0)
        }
    }
}