package com.app.weather.networking


import android.location.Location
import com.app.weather.responseModel.CurrentWeatherResponse
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.POST
import retrofit2.http.PATCH

/**
 * Service Interface to implement all the webservice
 * */
interface WeatherDataSource {

    @GET("/forecast/{access_key}/{location}")
    suspend fun getCurrentWeather(@Path("access_key") access_key: String,
                                  @Path("location") location: String,
                                  @Query("exclude") exclude : String)
            : CurrentWeatherResponse


}