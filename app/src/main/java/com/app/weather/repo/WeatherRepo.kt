package com.app.weather.repo

import com.app.weather.networking.Resource
import com.app.weather.responseModel.CurrentWeatherResponse

interface WeatherRepo {

    suspend fun getWeatherData(location: String) : Resource<CurrentWeatherResponse>

}
