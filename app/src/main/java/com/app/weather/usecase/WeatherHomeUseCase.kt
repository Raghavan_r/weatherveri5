package com.app.weather.usecase

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import com.app.weather.exception.ApiFailureException
import com.app.weather.networking.Status
import com.app.weather.repo.WeatherRepo
import com.app.weather.responseModel.CurrentWeatherResponse
import java.io.IOException
import java.util.*


class WeatherHomeUseCase (private val weatherRepo: WeatherRepo,
                          private val context: Context) {

    @Throws(ApiFailureException::class)
    suspend fun getWeatherUpdate(location: Location) : CurrentWeatherResponse? {
            weatherRepo.getWeatherData(location.latitude.toString()+","+location.longitude.toString()).apply {
                return when (this.status){
                    Status.SUCCESS -> {
                        this.data
                    }
                    Status.ERROR -> {
                        throw ApiFailureException(this.message)
                    }
                    else ->{
                        null
                    }
                }
            }
        }




    private fun getLocationUsingGeoCoder(location: Location): String? {

            val geocoder = Geocoder(
                context, Locale
                    .getDefault()
            )
            val addresses: List<Address>
            try {

                addresses = geocoder.getFromLocation(
                    location.latitude,
                    location.longitude,
                    1
                )
                return addresses[0].getAddressLine(0)

            } catch (e: IOException) { // TODO Auto-generated catch block
                e.printStackTrace()
                return null
            }


    }


}