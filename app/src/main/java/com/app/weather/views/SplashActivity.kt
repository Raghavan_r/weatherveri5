package com.app.weather.views

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.app.weather.R
import com.app.weather.databinding.ActivitySplashBinding
import com.app.weather.viewModel.SplashViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel

private const val DELAY_TIME = 3000

class SplashActivity : BaseActivity() {

    private val viewModel: SplashViewModel by viewModelScope?.viewModel(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding =
            DataBindingUtil.setContentView<ActivitySplashBinding>(this, R.layout.activity_splash)
        binding.splashViewModel = viewModel
        navigate()
    }

    private fun navigate() {

        GlobalScope.launch(Dispatchers.Main.immediate) {
            delay(DELAY_TIME.toLong())
            startActivity(viewModel.getIntent(this@SplashActivity))
            finish()
        }
    }
}
