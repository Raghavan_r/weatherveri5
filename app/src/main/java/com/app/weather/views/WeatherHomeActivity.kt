package com.app.weather.views

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.app.weather.R
import com.app.weather.adapter.DaysAdapter
import com.app.weather.databinding.ActivityWeatherHomeBinding
import com.app.weather.viewModel.WeatherHomeViewModel
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetBehavior
import org.koin.android.viewmodel.ext.android.viewModel


@Suppress("DEPRECATION")
class WeatherHomeActivity : BaseActivity() {

    private val viewModel: WeatherHomeViewModel by viewModelScope?.viewModel(this)
    private var daysAdapter = DaysAdapter()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding =
            DataBindingUtil.setContentView<ActivityWeatherHomeBinding>(
                this,
                R.layout.activity_weather_home
            )
        binding.homeViewModel = viewModel

        viewModel.startLocation()
        viewModel.validationError.observe(this, Observer {
            showErrorMessage(it, binding.root)
        })

        viewModel.weatherImg.observe(this, Observer {
            Glide.with(this)
                .load(it)
                .into(binding.ivTemp)
        })

        binding.adapter = daysAdapter

        viewModel.response.observe(this, Observer { res ->
            binding.tvCityname.text = res.timezone
            binding.tvTempType.text = res.currently?.icon
            binding.tvTemp.text = res.currently?.temperature.toString() + "\u2103"
            binding.tvSumdesc.text = res.currently?.summary

            res.daily?.data?.let { daysAdapter.setDataList(res.daily.data) }
        })

        val behavior: BottomSheetBehavior<*> = BottomSheetBehavior.from(binding.designBottomSheet)
        behavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                Log.i("BottomSheetCallback", "slideOffset: " + slideOffset);
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_DRAGGING -> Log.i(
                        "BottomSheetCallback",
                        "BottomSheetBehavior.STATE_DRAGGING"
                    )
                    BottomSheetBehavior.STATE_SETTLING -> Log.i(
                        "BottomSheetCallback",
                        "BottomSheetBehavior.STATE_SETTLING"
                    )
                    BottomSheetBehavior.STATE_EXPANDED -> Log.i(
                        "BottomSheetCallback",
                        "BottomSheetBehavior.STATE_EXPANDED"
                    )
                    BottomSheetBehavior.STATE_COLLAPSED -> Log.i(
                        "BottomSheetCallback",
                        "BottomSheetBehavior.STATE_COLLAPSED"
                    )
                    BottomSheetBehavior.STATE_HIDDEN -> Log.i(
                        "BottomSheetCallback",
                        "BottomSheetBehavior.STATE_HIDDEN"
                    )
                    else ->{

                    }
                }
            }

        })
        binding.btnDays.setOnClickListener {
            if (behavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else {
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        }



    }
}
